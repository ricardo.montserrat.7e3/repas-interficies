import java.util.Comparator;

public class Persona
{
    private String nom;
    private int edat;

    public Persona()
    {
        nom = "Desconegut";
        edat = 0;
    }

    public Persona(String nom, int edat)
    {
        this.nom = nom;
        this.edat = edat;
    }

    public String getNom()
    {
        return nom;
    }

    public int getEdat()
    {
        return edat;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setEdat(int edat)
    {
        this.edat = edat;
    }

    static class ComparatorPerNom implements Comparator<Persona>
    {
        @Override
        public int compare(Persona persona1, Persona persona2)
        {
            return persona1.getNom().length() - persona2.getNom().length();
        }
    }

    static class ComparatorPerEdat implements Comparator<Persona>
    {
        @Override
        public int compare(Persona persona1, Persona persona2)
        {
            return persona1.getEdat() - persona2.getEdat();
        }
    }

    @Override
    public String toString()
    {
        return nom + " " + edat;
    }

    public static int compararPerNom(Persona p1, Persona p2){
        return p1.getNom().compareTo(p2.getNom());
    }

}
