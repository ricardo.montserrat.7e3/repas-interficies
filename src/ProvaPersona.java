import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ProvaPersona
{
    public static void main(String[] args)
    {
        List<Persona> personaList = Arrays.asList(new Persona("Ricardo", 20), new Persona("Miquel", 18), new Persona("Javier", 19), new Persona("Adrian", 23), new Persona());

        System.out.println("Common List: ");

        System.out.println(personaList + "\n\nUsing Auxiliar Class\n-------------------------");

        System.out.println("\nSorted by name:");

        personaList.sort(new Persona.ComparatorPerNom());

        System.out.println(personaList);

        System.out.println("\nSorted by age:");

        personaList.sort(new Persona.ComparatorPerEdat());

        System.out.println(personaList + "\n\nUsing Anonymous Inside Class Class\n-------------------------");

        System.out.println("\nSorted by name:");

        personaList.sort(new Comparator<Persona>()
        {
            @Override
            public int compare(Persona o1, Persona o2)
            {
                return o1.getNom().length() - o2.getNom().length();
            }
        });

        System.out.println(personaList);

        System.out.println("\nSorted by age:");

        personaList.sort(new Comparator<Persona>()
        {
            @Override
            public int compare(Persona o1, Persona o2)
            {
                return o1.getEdat() - o2.getEdat();
            }
        });

        System.out.println(personaList + "\n\nUsing Lambda Expression\n-------------------------");

        System.out.println("\nSorted by name:");

        personaList.sort((o1, o2) -> o1.getNom().length() - o2.getNom().length());

        System.out.println(personaList);

        System.out.println("\nSorted by age:");

        personaList.sort((o1, o2) -> o1.getEdat() - o2.getEdat());

        System.out.println("\n\nSorting by age, or name if ages are equal\n-------------------------");

        personaList.sort(new Comparator<>()
        {
            @Override
            public int compare(Persona o1, Persona o2)
            {
                int difference = o1.getEdat() - o2.getEdat();
                if (difference == 0)
                    return o1.getNom().length() - o2.getNom().length();
                return difference;
            }
        });

        System.out.println("\n\nCalling Static Method\n-------------------------");

        personaList.sort(Persona::compararPerNom);
    }
}
